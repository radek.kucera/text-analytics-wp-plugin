<?php

namespace TextAnalytics\ContentLoaders;

use TextAnalytics\ContentLoaderInterface;
use TextAnalytics\Libraries\Pdf2text;

final class PdfFileContentLoader implements ContentLoaderInterface
{
    public function getContent($file): string
    {
        $converter = new Pdf2text(); 
        return $converter->decode($file['tmp_name']);
    }
}