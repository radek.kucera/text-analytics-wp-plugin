<?php

namespace TextAnalytics\ContentLoaders;

use TextAnalytics\ContentLoaderInterface;

final class TxtFileContentLoader implements ContentLoaderInterface
{
    public function getContent($file): string
    {
        return file_get_contents($file['tmp_name']);
    }
}
