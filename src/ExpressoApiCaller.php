<?php


namespace TextAnalytics;


use TextAnalytics\Libraries\CurlRequest;

final class ExpressoApiCaller
{
    /**
     * @param $text
     * @return array|mixed|object
     */
    public function analyseText($text)
    {
        $request = new CurlRequest('https://expresso-app.org/analyze-text');
        $response = $request->send([
            'html' => $text
        ]);

        return json_decode($response, true);
    }
}