<?php

namespace TextAnalytics;


interface ContentLoaderInterface
{
    public function getContent($file): string;
}