<?php

namespace TextAnalytics;


use Exception;
use InvalidArgumentException;
use WP_REST_Request;

final class AnalyticsApi
{
    /**
     * @var FileHandler
     */
    private $fileHandler;

    /**
     * @var ExpressoApiCaller
     */
    private $apiCaller;

    /**
     * Toggles that can be optionally turned off in admin settings
     * Such toggling will result into filtering the expresso API result
     * @var array
     */
    public static $toggles = [
        "adjective_ratio",
        "adverb_ratio",
        "bigram_freq",
        "character_count",
        "characters_per_word",
        "clauses_per_sentence",
        "complex_compound_ratio",
        "complex_ratio",
        "compound_ratio",
        "declarative_ratio",
        "detached_subjects_ratio",
        "entity_substitution_ratio",
        "exclamative_ratio",
        "filler_ratio",
        "fragment_ratio",
        "interrogative_ratio",
        "late_predicates_ratio",
        "long_noun_phrase_ratio",
        "long_sentences_ratio",
        "many_clauses_ratio",
        "modal_ratio",
        "negation_ratio",
        "nominalization_ratio",
        "noun_cluster_ratio",
        "noun_ratio",
        "other_pos_ratio",
        "passive_voice_ratio",
        "predicate_depth",
        "pronoun_ratio",
        "rare_word_ratio",
        "readability",
        "sentence_count",
        "short_sentences_ratio",
        "simple_ratio",
        "std_of_words_per_sentence",
        "stopword_ratio",
        "syllables_per_word",
        "trigram_freq",
        "verb_ratio",
        "vocabulary_size",
        "weak_verb_ratio",
        "word_count",
        "word_freq",
        "words_per_sentence",
        "auxiliary_verbs",
        "base_lemmas",
        "clause_heavy_sentences",
        "detached_subjects",
        "entity_substitutions",
        "expected_word_frequencies",
        "filler_words",
        "independent_principal_parts",
        "late_predicates",
        "lemmas",
        "long_noun_phrases",
        "negations",
        "nominalizations",
        "noun_clusters",
        "numbers_of_characters",
        "numbers_of_syllables",
        "parts_of_speech",
        "passive_voice_cases",
        "pos_high_level",
        "principal_parts",
        "punctuation_marks",
        "sentence_end_punctuations",
        "sentence_numbers",
        "sentence_types",
        "stopwords",
        "synonyms",
        "syntax_relations",
        "values",
        "verb_groups",
        "weak_verbs",
        "words"
    ];

    /**
     * AnalyticsApi constructor.
     */
    public function __construct()
    {
        $this->fileHandler = new FileHandler();
        $this->apiCaller = new ExpressoApiCaller();
    }

    /**
     * @param WP_REST_Request $request
     * @return array
     */
    public function handleTextRequest(WP_REST_Request $request)
    {
        header('Content-type: application/json');

        $parameters = $request->get_json_params();

        if (!array_key_exists('text', $parameters))
        {
            throw new InvalidArgumentException('Text parameter not present.');
        }

        return $this->runAnalytics($parameters['text']);
    }

    /**
     * @param WP_REST_Request $request
     * @return array
     */
    public function handleFileRequest(WP_REST_Request $request)
    {
        header('Content-type: application/json');

        $content = $this->retrieveFileContent($request);

        if ($content['status'] === false)
        {
            return $content;
        }

        return $this->runAnalytics($content['content']);
    }

    /**
     *
     */
    private function runAnalytics($content)
    {
        $data = $this->apiCaller->analyseText($content);

        return $this->filterApiResponse($data);
    }
    /**
     * @param WP_REST_Request $request
     * @return array
     */
    private function retrieveFileContent(WP_REST_Request $request)
    {
        try
        {
            $files = $request->get_file_params();

            if (!isset($files['file']))
            {
                throw new InvalidArgumentException("Missing argument file");
            }

            $content = $this->fileHandler->getFileContent($files['file']);
        }
        catch(Exception $exception)
        {
            return [
                'status' => false,
                'error' => $exception->getMessage(),
            ];
        }

        return [
            'status' => true,
            'content' => $content
        ];
    }

    /**
     * @param array $data
     * @return array
     */
    private function filterApiResponse($data)
    {
        foreach (static::$toggles as $toggle)
        {
            $enabled = get_option($toggle) === 'true';

            if (!$enabled)
            {
                if (isset($data['metrics'][$toggle]))
                {
                    unset($data['metrics'][$toggle]);
                }

                if (isset($data['tokens'][$toggle]))
                {
                    unset($data['tokens'][$toggle]);
                }
            }
        }

        return $data;
    }
}
