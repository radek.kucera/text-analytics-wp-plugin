<?php

namespace TextAnalytics;

use Exception;
use InvalidArgumentException;
use TextAnalytics\ContentLoaders\DocxFileContentLoader;
use TextAnalytics\ContentLoaders\PdfFileContentLoader;
use TextAnalytics\ContentLoaders\TxtFileContentLoader;
use UnexpectedValueException;
use WP_REST_Request;

final class FileHandler
{
    // Do not allow text longer than 5000 chars
    private $limit = 5000;

    private $registeredLoaders = [
        'text/plain' => TxtFileContentLoader::class,
        'application/pdf' => PdfFileContentLoader::class,
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => DocxFileContentLoader::class
    ];

    /**
     * @param $file
     * @return mixed
     */
    public function getFileContent($file)
    {
        $type = $file['type'];

        if (!array_key_exists($type, $this->registeredLoaders))
        {
            throw new UnexpectedValueException("File type [$type] cannot be handled.");
        }

        /** @var ContentLoaderInterface */
        $loader = new $this->registeredLoaders[$type];
        $content = $loader->getContent($file);

        if (strlen($content) > $this->limit)
        {
            throw new InvalidArgumentException("The provided file contains text that exceeds limit of 5000 characters.");
        }

        return $content;
    }
}