<?php

namespace TextAnalytics;


final class Plugin
{
    private const NAME = 'text-analytics';

    private $baseDirectory;

    public function run()
    {
        $this->registerWorpressHooks();
        $this->registerRestRoutes();
        $this->registerGutenbersBlock();
    }

    private function registerWorpressHooks()
    {
        $this->baseDirectory = plugins_url(static::NAME . '/public/');

        add_action('init', [$this, 'registerSettings']);

        add_action('admin_enqueue_scripts', [$this, 'enqueueScripts']);
        add_action('wp_enqueue_script', [$this, 'enqueueScripts']);

        add_action('admin_print_styles', [$this, 'enqueueStyles']);
        add_action('wp_print_styles', [$this, 'enqueueStyles']);

        add_action('admin_menu', [$this, 'createMenuPage']);

        add_action('wp_head', [$this, 'enqueueScriptsToPage']);
        add_action('wp_head', [$this, 'enqueueStylesToPage']);
    }

    private function registerRestRoutes()
    {
        $analyticsApi = new AnalyticsApi();

        add_action('rest_api_init', function () use ($analyticsApi) {
            register_rest_route(
                'text-analytics',
                'analyse-file',
                [
                    'methods' => 'POST',
                    'callback' => [$analyticsApi, 'handleFileRequest']
                ]
            );

            register_rest_route(
                'text-analytics',
                'analyse-text',
                [
                    'methods' => 'POST',
                    'callback' => [$analyticsApi, 'handleTextRequest']
                ]
            );
        });
    }

    private function registerGutenbersBlock()
    {
        $registrationClosure = static function () {
            register_block_type(
                self::NAME . '/analysed-text-block',
                [
                    'editor_script' => 'text-analytics-block-js',
                ]
            );
        };

        add_action('init', $registrationClosure);
    }

    public function registerSettings()
    {
        foreach (AnalyticsApi::$toggles as $key)
        {
            register_setting( self::NAME, $key, [
                'type' => 'boolean',
                'default' => true
            ]);
        }
    }

    public function createMenuPage()
    {
        add_menu_page('Text analytics', 'Text analytics', 4, 'text-analytics',
            static function () {
                ob_start();

                $toggles = AnalyticsApi::$toggles;

                require (__DIR__ . '/../public/html/admin.phtml');
                echo ob_get_clean();
            }
        );
    }

    public function enqueueScripts()
    {
        wp_enqueue_script('text-analytics-block-js', $this->baseDirectory . 'js/block.js', ['wp-blocks', 'wp-element']);
    }

    public function enqueueScriptsToPage()
    {
        wp_enqueue_script('text-analytics-block-js', $this->baseDirectory . 'js/block.js', ['wp-blocks', 'wp-element']);
        wp_enqueue_script('text-analytics-js', $this->baseDirectory . 'js/plugin.js', ['wp-blocks', 'wp-element']);
        wp_enqueue_script('mark-js-lib', 'https://cdnjs.cloudflare.com/ajax/libs/mark.js/8.11.1/mark.min.js');
    }

    public function enqueueStyles()
    {
        wp_enqueue_style('text-analytics-css', $this->baseDirectory . 'css/plugin.css');
    }

    public function enqueueStylesToPage()
    {
        wp_enqueue_style('text-analytics-view-css', $this->baseDirectory . 'css/view.css');
    }
}
