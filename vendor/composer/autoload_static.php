<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit2b9329025c6d3e6b1026962eb2b15c86
{
    public static $prefixLengthsPsr4 = array (
        'T' => 
        array (
            'TextAnalytics\\' => 14,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'TextAnalytics\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit2b9329025c6d3e6b1026962eb2b15c86::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit2b9329025c6d3e6b1026962eb2b15c86::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
