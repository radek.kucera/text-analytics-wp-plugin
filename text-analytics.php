<?php
/**
 * @package Text analytics
 * @version 1.0.3
 */
/*
Plugin Name: Text analytics
Plugin URI: http://radakuceru.eu/text-analytics-plugin
Description: This plugin represents custom widget that helps you to analyse you text. Supported files: .pdf, .docx, .txt. 
Author: Radek Kučera, Jiří Vrba
Version: 1.0.3
Author URI: http://radakuceru.eu
*/

if (!defined('WPINC'))
{
    exit;
}

require __DIR__ . '/vendor/autoload.php';

use TextAnalytics\Plugin;

$instance = new Plugin();
$instance->run();
