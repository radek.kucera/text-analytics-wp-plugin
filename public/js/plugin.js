﻿﻿  "use strict";

  (function() {
    const ReactDOM_render = wp.element.render;
    const render = wp.element.createElement;
    const API_URL = "./?rest_route=/text-analytics/analyse-file";
    const API_URL_TEXT = "./?rest_route=/text-analytics/analyse-text";

    const _createContent = Symbol("_createContent");
    const _makeFormData = Symbol("_makeFormData");
    const _highlightWords = Symbol("_highlightWords");
    const _removeHighlight = Symbol("_removeHighlight");
    const _createTable = Symbol("_createTable");
    const _createTextField = Symbol("_createTextField");

    window.addEventListener("load", main);

    function main() {
      let input = document.getElementById("text-analyse__file-input");
      input.addEventListener("change", main_file_input);

      let form = document.getElementById("text-analyse__user_form_submit");
      form.addEventListener("click", main_form_input);
    }

    async function main_form_input() {
      let input = document.getElementById("text-analyse__user_form_textarea");

      let body = JSON.stringify({
        text: input.value
      });

      let res = await fetch(API_URL_TEXT, {
        method: "POST",
        headers: {
          "Content-type": "application/json"
        },
        body: body
      });

      let analyzed_data = await res.json();

      if (analyzed_data.status != false) {
        let textAnalyser = await new TextAnalyser(analyzed_data);
        let statistics = textAnalyser.mapStatistics();

        let textAnalyserView = await new TextAnalyserView(
          statistics,
          textAnalyser.text
        );

        textAnalyserView.renderContent();
      } else {
        alert(analyzed_data.error);
      }
    }

    async function main_file_input() {
      let input = document.getElementById("text-analyse__file-input");
      let apiCaller = new TextAnalyserAPI(input.files[0], API_URL);

      let analyzed_data = await apiCaller.sendRequest().then(res => {
        return res;
      });

      if (analyzed_data.status !== false) {
        let textAnalyser = await new TextAnalyser(analyzed_data);
        let statistics = textAnalyser.mapStatistics();

        let textAnalyserView = await new TextAnalyserView(
          statistics,
          textAnalyser.text
        );

        textAnalyserView.renderContent();
      } else {
        alert(analyzed_data.error);
      }
    }

    class TextAnalyserView {
      constructor(statistics, text) {
        this.statistics = statistics;
        this.text = text;
      }

      renderContent() {
        ReactDOM_render(
          this[_createTable](),
          document.getElementById("text-analyse__output")
        );
      }

      [_createContent](key, item) {
        if (item.value !== undefined) {
          let formatted_key = key.replace(/^\w/, l => l.toUpperCase());
          return render(
            "tr",
            {
              id: "text-analyse__items_key",
              className:
                item.tokens !== undefined
                  ? "text-analyse__items_with_hover"
                  : null,
              /*

                HOVER: 

                onMouseOver: () => {
                  this[_highlightWords](item.tokens);
                },
                onMouseLeave: () => {
                  this[_removeHighlight]();
                },
                */
              onClick: async () => {
                await this[_removeHighlight]();
                await this[_highlightWords](item.tokens);
              }
            },
            [
              render("td", {}, formatted_key.replace(/_/g, " ")),
              render("td", {}, item.value)
            ]
          );
        }
      }

      [_highlightWords](tokens) {
        let mark = new Mark(document.getElementById("text-analyse__output_text"));

        if (tokens !== undefined) {
          for (let i in tokens) {
            let item = tokens[i];
            if (item != null && item != false) {
              mark.mark(this.statistics.values.tokens[i], {
                accuracy: {
                  value: "exactly",
                  limiters: []
                },
                element: "span",
                className: "highlight"
              });
            }
          }
        }
      }

      [_removeHighlight]() {
        let mark = new Mark(document.getElementById("text-analyse__output_text"));
        mark.unmark("element");
      }

      [_createTable]() {
        let components = [];

        for (let key in this.statistics) {
          let item = this.statistics[key];
          if (item.value !== undefined && item.value != null)
            components.push(this[_createContent](key, item));
        }

        return render("div", {}, [
          this[_createTextField](),
          render(
            "table",
            {
              id: "text-analyse__output_table"
            },
            components
          )
        ]);
      }

      [_createTextField]() {
        let word_array = [];
        let array = this.statistics.values.tokens;
        let text = "";

        let hover_elem = render(
          "div",
          {
            id: "analyse__output_synonyms_hover",
            className: "analyse__output_synonyms_hover"
          },
          ""
        );

        for (let i = 0; i < array.length; i++) {
          if (array[i + 1] && /\W/.test(array[i + 1])) {
            text = array[i];
          } else {
            text = array[i] + " ";
          }

          let elem = render(
            "span",
            {
              className: "analyse__output_text_words",
              onMouseOver: event => {
                let x = event.clientX;
                let y = event.clientY;
                let text = "";
                let array = this.statistics.synonyms.tokens[i];
                if (array != null && JSON.stringify(array) != "[]") {
                  for (let j = 0; j < array.length; j++) {
                    if (j == array.length - 1) {
                      text += array[j];
                    } else text += array[j] + ", ";
                  }
                  let hover = document.getElementById(
                    "analyse__output_synonyms_hover"
                  );
                  hover.innerText = text;
                  hover.style.top = y + 16 + "px";
                  hover.style.left = x + "px";
                  hover.style.display = "inline-block";
                }
              },
              onMouseLeave: () => {
                let hover = document.getElementById(
                  "analyse__output_synonyms_hover"
                );
                hover.style.display = "none";
              }
            },
            text
          );
          word_array.push(elem);
        }
        let p_elem = render("p", { id: "text-analyse__output_text" }, word_array);
        return render("div", { id: "text-analyse__output_text" }, [
          p_elem,
          hover_elem
        ]);
      }
    }

    class TextAnalyser {
      constructor(data) {
        this.metrics = data.metrics;
        this.text = data.text;
        this.tokens = data.tokens;
      }

      converters = {
        percentage: value => {
          if (typeof value == "number") {
            if (value !== 0) {
              return (value * 100).toFixed(2) + "%";
            } else return "0%";
          } else return undefined;
        },
        round: value => {
          if (typeof value == "number") {
            if (value !== 0) {
              return ((value * 10) / 10).toFixed(2);
            } else return "0";
          } else return undefined;
        },
        minusRound: value => {
          if (typeof value == "number") {
            if (value >= 0) {
              return ((value * 10) / 10).toFixed(2);
            } else return "-";
          } else return undefined;
        },
        arrayLength: value => value.length,
        checkPartsOfSpeech: (value, type) => {
          if (value !== undefined) {
            let array = [];
            for (let i in value) {
              if (type.indexOf(value[i].slice(0, 2)) !== -1) {
                array.push(value[i]);
              } else array.push(null);
            }
            return array;
          } else return value;
        },
        checkOtherPartsOfSpeech: (value, type) => {
          if (value !== undefined) {
            let array = [];
            for (let i in value) {
              if (type.indexOf(value[i].slice(0, 2)) === -1) {
                array.push(value[i]);
              } else array.push(null);
            }
            return array;
          } else return value;
        },
        checkTypeOfSentence: (value, type) => {
          if (value !== undefined) {
            let re_array = value.reverse();
            let word_array = [];

            for (let i in re_array) {
              let item = re_array[i];
              if (type.indexOf(item) !== -1) {
                word_array.unshift(item);
              } else word_array.unshift(null);
            }

            return word_array;
          } else return value;
        },
        itemEqualNumber: (value, num) => {
          if (value !== undefined) {
            let array = [];
            for (let i in value) {
              if (value[i] === num) {
                array.push(value[i]);
              } else value.push(null);
            }
            return array;
          } else return value;
        },
        showFreq: value => {
          if (value !== undefined) {
            let comp_array = [];
            let text = "";
            for (let i in value) {
              let item = value[i];
              let words = "";
              if (typeof item[0] == "string") {
                words = item[0];
              } else {
                for (let i in item[0]) {
                  words += item[0][i] + " ";
                }
              }
              text = `${words}: ${item[1]} \n`;

              comp_array.push(
                render(
                  "span",
                  {
                    key: item[0],
                    className: "text-analyse__output_freq_item",
                    onClick: async () => {
                      let mark = await new Mark(
                        document.getElementById("text-analyse__output_text")
                      );
                      await mark.unmark("element");
                      await mark.mark(item[0], {
                        element: "span",
                        className: "highlight"
                      });
                    }
                  },
                  text
                )
              );
            }
            return render("div", {}, comp_array);
          } else return value;
        }
      };

      mapStatistics() {
        return {
          values: {
            tokens: this.tokens.values
          },
          characters: {
            value: this.metrics.character_count
          },
          words: {
            value: this.metrics.word_count,
            tokens: this.tokens.words
          },
          vocabulary_size: {
            value: this.metrics.vocabulary_size
          },
          sentence_count: {
            value: this.metrics.sentence_count
          },
          clauses_per_sentence: {
            value: this.converters.round(this.metrics.clauses_per_sentence),
            tokens: this.tokens.principal_parts
          },
          predicate_depth: {
            value: this.converters.round(this.metrics.predicate_depth),
            tokens: this.tokens.independent_principal_parts
          },
          words_per_sentence: {
            value: this.converters.round(this.metrics.words_per_sentence)
          },
          syllables_per_word: {
            value: this.converters.round(this.metrics.syllables_per_word)
          },
          std_of_words_per_sentence: {
            value: this.converters.minusRound(
              this.metrics.std_of_words_per_sentence
            )
          },
          characters_per_word: {
            value: this.converters.round(this.metrics.characters_per_word)
          },
          readability_grade: {
            value: this.converters.round(this.metrics.readability)
          },
          nouns: {
            value: this.converters.percentage(this.metrics.noun_ratio),
            tokens: this.converters.checkPartsOfSpeech(
              this.tokens.parts_of_speech,
              ["NN"]
            )
          },
          pronouns: {
            value: this.converters.percentage(this.metrics.pronoun_ratio),
            tokens: this.converters.checkPartsOfSpeech(
              this.tokens.parts_of_speech,
              ["PR", "WP", "EX"]
            )
          },
          verbs: {
            value: this.converters.percentage(this.metrics.verb_ratio),
            tokens: this.converters.checkPartsOfSpeech(
              this.tokens.parts_of_speech,
              ["VB", "MD"]
            )
          },
          adjectives: {
            value: this.converters.percentage(this.metrics.adjective_ratio),
            tokens: this.converters.checkPartsOfSpeech(
              this.tokens.parts_of_speech,
              ["JJ"]
            )
          },
          adverbs: {
            value: this.converters.percentage(this.metrics.adverb_ratio),
            tokens: this.converters.checkPartsOfSpeech(
              this.tokens.parts_of_speech,
              ["RB"]
            )
          },
          other_parts_of_speech: {
            value: this.converters.percentage(this.metrics.other_pos_ratio),
            tokens: this.converters.checkOtherPartsOfSpeech(
              this.tokens.parts_of_speech,
              ["NN", "PR", "WP", "VB", "JJ", "RB", "MD"]
            )
          },
          declarative_sentences: {
            value: this.converters.percentage(this.metrics.declarative_ratio),
            tokens: this.converters.checkTypeOfSentence(
              this.tokens.sentence_end_punctuations,
              [".", "..."]
            )
          },
          interrogative_sentences: {
            value: this.converters.percentage(this.metrics.interrogative_ratio),
            tokens: this.converters.checkTypeOfSentence(
              this.tokens.sentence_end_punctuations,
              ["?"]
            )
          },
          exclamative_sentences: {
            value: this.converters.percentage(this.metrics.exclamative_ratio),
            tokens: this.converters.checkTypeOfSentence(
              this.tokens.sentence_end_punctuations,
              ["!"]
            )
          },
          simple_sentences: {
            value: this.converters.percentage(this.metrics.simple_ratio),
            tokens: this.converters.checkTypeOfSentence(
              this.tokens.sentence_types,
              ["simple"]
            )
          },
          complex_sentences: {
            value: this.converters.percentage(this.metrics.complex_ratio),
            tokens: this.converters.checkTypeOfSentence(
              this.tokens.sentence_types,
              ["complex"]
            )
          },
          compound_sentences: {
            value: this.converters.percentage(this.metrics.compound_ratio),
            tokens: this.converters.checkTypeOfSentence(
              this.tokens.sentence_types,
              ["compound"]
            )
          },
          complex_compound_sentences: {
            value: this.converters.percentage(
              this.metrics.complex_compound_ratio
            ),
            tokens: this.converters.checkTypeOfSentence(
              this.tokens.sentence_types,
              ["complex-compound"]
            )
          },
          stopwords: {
            value: this.converters.percentage(this.metrics.stopword_ratio),
            tokens: this.tokens.stopwords
          },
          weak_verbs: {
            value: this.converters.percentage(this.metrics.weak_verb_ratio),
            tokens: this.tokens.weak_verbs
          },
          fillers: {
            value: this.converters.percentage(this.metrics.filler_ratio),
            tokens: this.tokens.filler_words
          },
          nominalizations: {
            value: this.converters.percentage(this.metrics.nominalization_ratio),
            tokens: this.tokens.nominalizations
          },
          entity_substitution: {
            value: this.converters.percentage(
              this.metrics.entity_substitution_ratio
            ),
            tokens: this.tokens.entity_substitution
          },
          negations: {
            value: this.converters.round(this.metrics.negation_ratio),
            tokens: this.tokens.negations
          },
          clustered_nouns: {
            value: this.converters.percentage(this.metrics.noun_cluster_ratio)
          },
          long_noun_phrases: {
            value: this.converters.percentage(this.metrics.long_noun_phrase_ratio)
          },
          passive_voice_per_sentence: {
            value: this.converters.round(this.metrics.passive_voice_ratio),
            tokens: this.tokens.passive_voice_cases
          },
          modals: {
            value: this.converters.percentage(this.metrics.modal_ratio),
            tokens: this.converters.checkOtherPartsOfSpeech(
              this.tokens.parts_of_speech,
              ["MD"]
            )
          },
          rare_words: {
            value: this.converters.percentage(this.metrics.rare_word_ratio),
            tokens: this.converters.itemEqualNumber(
              this.tokens.expected_word_frequencies,
              0
            )
          },
          long_sentences: {
            value: this.converters.percentage(this.metrics.long_sentences_ratio)
          },
          short_sentences: {
            value: this.converters.percentage(this.metrics.short_sentences_ratio)
          },
          fragments: {
            value: this.converters.percentage(this.metrics.fragment_ratio),
            tokens: this.converters.checkTypeOfSentence(
              this.tokens.sentence_types,
              ["fragment"]
            )
          },
          clause_heavy_sentences: {
            value: this.converters.percentage(this.metrics.many_clauses_ratio),
            tokens: this.tokens.clause_heavy_sentences
          },
          late_predicates: {
            value: this.converters.percentage(this.metrics.late_predicates_ratio),
            tokens: this.tokens.late_predicates
          },
          detached_subjects: {
            value: this.converters.percentage(
              this.metrics.detached_subjects_ratio
            ),
            tokens: this.tokens.detached_subjects
          },
          frequent_words: {
            value: this.converters.showFreq(this.metrics.word_freq)
          },
          frequent_bigrams: {
            value: this.converters.showFreq(this.metrics.bigram_freq)
          },
          frequent_trigrams: {
            value: this.converters.showFreq(this.metrics.trigram_freq)
          },
          synonyms: {
            tokens: this.tokens.synonyms
          }
        };
      }
    }

    class TextAnalyserAPI {
      constructor(file, url) {
        this.file = file;
        this.url = url;
      }

      [_makeFormData]() {
        let formData = new FormData();
        formData.append("file", this.file);
        return formData;
      }

      async sendRequest() {
        let formData = this[_makeFormData]();

        const res = await fetch(this.url, {
          method: "POST",
          body: formData
        });

        return await res.json();
      }
    }
  })();
