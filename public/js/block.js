"use strict";

(function() {
  const { registerBlockType } = wp.blocks;
  const el = wp.element.createElement;

  registerBlockType("text-analytics/analysed-text-block", {
    title: "Text analyser",
    icon: "format-aside",
    category: "widgets",

    edit() {
      return el(
        "div",
        {
          className: "text-analyser-preview"
        },
        [
          el(
            "div",
            {
              className: "text-analyser-preview__title"
            },
            "Text analyser"
          ),
          el(
            "div",
            {
              className: "text-analyser-preview__input"
            },
            "File input"
          ),
          el(
            "div",
            {
              className: "text-analyser-preview__button"
            },
            "Submit"
          )
        ]
      );
    },

    save() {
      return el(
        "div",
        {
          className: "text-analyser"
        },
        [
          el(
            "h1",
            {
              className: "text-analyser"
            },
            "Text analyser"
          ),
          el(
            "input",
            {
              id: "text-analyse__file-input",
              type: "file"
            },
            ""
          ),
          el("span", {}, "OR"),
          el(
            "div",
            {
              id: "text-analyse__user_form"
            },
            [
              el(
                "textarea",
                {
                  id: "text-analyse__user_form_textarea",
                  name: "text-analyse__user_form_text",
                  form: "text-analyse__user_form",
                  className: "text-analyse__user_form_textarea"
                },
                ""
              ),
              el(
                "input",
                {
                  type: "submit",
                  id: "text-analyse__user_form_submit",
                  className: "text-analyse__user_form_submit"
                },
                "Submit"
              )
            ]
          ),
          el("div", {
            id: "text-analyse__output"
          })
        ]
      );
    }
  });
})();
